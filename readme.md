# Linux Scripts
This is a small collection of Python & Shell scripts I've written to make life easier for myself.

## Overview

- backup:
    - veeam-backup-rotation
- general:
    - download-playlist
    - filter-csv
    - sort-graylog-csv
- security:
    - configure-openssl-ca
    - convert-ssl
    - copy-ssl
    - genpwd
    - hasmycompanybeenpwned
