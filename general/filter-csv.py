#!/usr/bin/env python3
"""
This script was written to filter a csv file of 1500 lines, using another csv file of 500 lines as search criteria.
Both csv's are read into memory and copied into dictionaries, which are then combined into a filtered dictionary.
Finally, this filtered dictionary's contents are written to a new csv file.
"""
import csv
import os

filename = 'staff-mailboxes.csv'

if os.path.exists(filename):
    os.remove(filename)

with open(filename, 'w+') as target_file:
    w = csv.writer(target_file)
    w.writerow(['name','email','mailbox size'])

with open('mailboxes.csv','r') as mailboxes:
    reader = csv.reader(mailboxes)
    mailbox_dictionary = {rows[0]:rows[1] for rows in reader}

with open('staff.csv','r') as staff:
    reader = csv.reader(staff)
    staff_dictionary = {rows[0]:rows[1] for rows in reader}

filtered_dictionary = {k: v for k, v in mailbox_dictionary.items() if k in staff_dictionary.keys()}

with open(filename, 'w+') as target_file:
    w = csv.writer(target_file)
    w.writerow(['Email', 'Size'])
    for k, v in filtered_dictionary.items():
        w.writerow([k,v])
