#!/usr/bin/env python3
"""
This script will download a single song or all tracks in a playlist from youtube and concatenate them with ffmpeg.
"""
from __future__ import unicode_literals
import os
import subprocess
import sys
import youtube_dl

codec = 'mp3'
textfile = 'playlist_items.txt'
ydl_opts = {
    'format': 'bestaudio/best',
    'outtmpl': '%(id)s.%(ext)s',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': codec,
        'preferredquality': '192',
    }]
}
with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    playlist_dict = ydl.extract_info(sys.argv[1], download=True)
    if 'entries' in playlist_dict: # playlist
        playlist = open(textfile, 'w+')
        for item in playlist_dict['entries']:
            playlist.write("file '" +  os.path.join(os.path.dirname(os.path.realpath(__file__)), item.get('id')) + "." + codec + "'\n")
        playlist.close()
        subprocess.call(['ffmpeg', '-y', '-f', 'concat', '-safe', '0', '-i', textfile, '-c', 'copy', playlist_dict['title'] + '.' + codec])
        playlist = open(textfile, 'r')
        for line in playlist:
            filename = line.strip("file '").strip("'\n")
            os.remove(filename)
        playlist.close()
        os.remove(textfile)
    else: # single song
        os.rename(playlist_dict['id'] + '.' + codec, playlist_dict['title'] + '.' + codec)
