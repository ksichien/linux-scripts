#!/usr/bin/env python3
"""
This script was written to extract data from csv files exported by graylog.
The exported csv files were mail server logs where each file had over 5000 lines of the following format:
"2018-01-01T01:01:01.111Z","mail.vandelayindustries.com","imap-login: Login: user=<art.vandelay>, method=PLAIN, rip=X.X.X.X, lip=X.X.X.X, mpid=1000, TLS, session=<XXXXXXXXXXXXXX>"
The script's purpose was to find all unique netnames from which a user had logged in to the mail server.
"""
import csv
import os
import subprocess
users = ['art.vandelay','kel.varnsen']
for user in users:
    if os.path.exists(f'{user}.csv'):
        os.remove(f'{user}.csv')
    if os.path.exists(f'{user}-fails.csv'):
        os.remove(f'{user}-fails.csv')
    if os.path.exists(f'{user}-locations.csv'):
        os.remove(f'{user}-locations.csv')
    if os.path.exists(f'{user}-bad-locations.csv'):
        os.remove(f'{user}-bad-locations.csv')
    print(f'Processing user {user}.')
    with open(f'{user}.csv', 'w+') as target_file:
        w = csv.writer(target_file)
        w.writerow(['timestamp','user','ip'])
    with open(f'{user}-fails.csv', 'w+') as target_file:
        w = csv.writer(target_file)
        w.writerow(['timestamp','user','ip'])
    with open(f'graylog-{user}.csv', 'r') as source_file:
        csv_reader = csv.DictReader(source_file)
        for row in csv_reader:
            if 'Disconnected' not in row['message']:
                prefix = row['message'][25:-1]
                middle = prefix.replace('>, method=PLAIN, rip=', ',')
                suffix = middle[0:middle.index('lip')]
                message = suffix.replace(', ', '')
                with open(f'{user}.csv', 'a+') as target_file:
                    target_file.writerow(f'{row["timestamp"]},{message}')
            else:
                prefix = row['message'][69:-1]
                middle = prefix.replace('>, method=PLAIN, rip=', ',')
                suffix = middle[0:middle.index('lip')]
                message = suffix.replace(', ', '')
                with open(f'{user}-fails.csv', 'a+') as target_file:
                    target_file.writerow(f'{row["timestamp"]},{message}')
    print('Finished sorting the csv file.')
    with open(f'{user}.csv', 'r') as source_file:
        csv_reader = csv.DictReader(source_file)
        locations = []
        for row in csv_reader:
            if row['ip'] not in locations:
                locations.append(row['ip'])
                with open(f'{user}-locations.csv', 'a+') as target_file:
                    target_file.writerow(row['ip'])
    with open(f'{user}-fails.csv', 'r') as source_file:
        csv_reader = csv.DictReader(source_file)
        locations = []
        for row in csv_reader:
            if row['ip'] not in locations:
                locations.append(row['ip'])
                with open(f'{user}-bad-locations.csv', 'a+') as target_file:
                    target_file.writerow(row['ip'])
    print('Finished filtering the csv file.')
